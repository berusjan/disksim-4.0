# Makefile for DiskSim


NAME_DISKSIM_BASE	= disksim-4.0.tar.gz
NAME_DISKSIM_DIXTRAC	= disksim-4.0-with-dixtrac.tar.gz
NAME_SSD		= ssd-add-on.zip
NAME_ARCH64  		= PFSsim-master.zip

# local directories
DIR_PATCH           = patches
DIR_SRC             = src
DIR_BUILD           = build
INSTALLDIR          = bin
DIR_DISKSIM         = $(DIR_BUILD)/disksim-4.0
DIR_SSD             = $(DIR_DISKSIM)/ssdmodel
DIR_ARCH64          = $(DIR_BUILD)/PFSsim-master

# downloaded files
FILE_DISKSIM_BASE        = $(DIR_SRC)/$(notdir $(NAME_DISKSIM_BASE))
FILE_DISKSIM_DIXTRAC     = $(DIR_SRC)/$(notdir $(NAME_DISKSIM_DIXTRAC))
FILE_SSD          	 = $(DIR_SRC)/$(notdir $(NAME_SSD))
FILE_ARCH64         	 = $(DIR_SRC)/$(notdir $(NAME_ARCH64))

# other files
EXEC                = $(DIR_DISKSIM)/src/disksim
PATCHED             = $(DIR_BUILD)/.patched

# commands
MAKE                = make
CHMOD               = chmod
CURL                = curl -skLO
UNTAR               = tar xzf
RMDIR               = rm -rf
MKDIR               = mkdir -p
UNZIP               = unzip -o -q
PATCH               = patch -p1
SED                 = sed
FIND                = find
OD                  = od -c
GREP                = grep
SPLITDIFF           = splitdiff -ad
RMFILE              = rm -f
CP                  = cp -a
LS                  = ls -l
STRIP               = strip
TOUCH               = touch
FILE_CMD            = file -ib
ECHO                = @/bin/echo -e
TITLE               = @/bin/echo -en "\n$(COLOR_TITLE)"; /bin/echo -n
SUCCESS             = @/bin/echo -en "\n$(COLOR_OK)"; /bin/echo -n
END                 = ; /bin/echo -e "$(COLOR_NONE)"

# colors
COLOR_NONE          = \x1b[0m
COLOR_TITLE         = \x1b[33;01m
COLOR_SUMMARY       = \x1b[35;01m
COLOR_OK            = \x1b[32;01m

# dynamic target lists
VALIDATION_TESTS    = $(DIR_DISKSIM)/valid/runvalid $(DIR_DISKSIM)/valid/memsvalid



# main
.DEFAULT: all
.PHONY: all
all: compile install test

# create folders
$(INSTALLDIR):
	$(MKDIR) '$@'


# compile disksim
.PHONY: compile
compile $(EXEC): $(PATCHED) | $(DIR_DISKSIM)
	$(TITLE) '[DISKSIM] Compile ...' $(END)
	$(CHMOD) +x '$|/libparam/'*.pl
	$(MAKE) -C '$|'
	test -x '$(EXEC)'
	$(SUCCESS) 'OK, compiled $(EXEC)' $(END)


# install
.PHONY: install
install: $(EXEC) | $(INSTALLDIR)
	$(TITLE) '[DISKSIM] Install binaries ...' $(END)
	@for f in $$($(FIND) $(DIR_DISKSIM)/src $(DIR_DISKSIM)/dixtrac $(DIR_DISKSIM)/memsmodel -maxdepth 1 -type f -executable); do \
	  case "$$($(FILE_CMD) "$$f")" in \
  	  'application/x-executable'*) \
         echo "$(CP) $$f $|/"; \
         $(CP) "$$f" '$|'/;; \
    esac; \
	done;
	cd '$|' && $(STRIP) *
	$(SUCCESS) 'OK, installed binaries:' $(END)
	@$(LS) '$(INSTALLDIR)/'* | $(GREP) -v '^total'
	$(ECHO)


# tests
.PHONY: test
test: | $(VALIDATION_TESTS)

.PHONY: $(VALIDATION_TESTS)
$(VALIDATION_TESTS): $(EXEC)
	$(TITLE) '[DISKSIM] Run validation test $@ ...' $(END)
	cd '$(dir $@)' && sh '$(notdir $@)'


# clean
.PHONY: clean
clean:
	$(RMDIR) '$(INSTALLDIR)'



# help
.PHONY: help
help:
	$(ECHO) '$(COLOR_OK)Make patched DiskSim$(COLOR_NONE)'
	$(ECHO)
	$(ECHO) '$(COLOR_TITLE)Usage:$(COLOR_NONE)'
	$(ECHO) '  make [target ...]'
	$(ECHO)
	$(ECHO) '$(COLOR_TITLE)Targets:$(COLOR_NONE)'
	$(ECHO) '          all ......... make compile, install and test'
	$(ECHO) '          compile ..... compile disksim in $(COLOR_SUMMARY)$(DIR_DISKSIM)/$(COLOR_NONE)'
	$(ECHO) '          install ..... install binaries to $(COLOR_SUMMARY)$(INSTALLDIR)/$(COLOR_NONE)'
	$(ECHO) '          test ........ run valadation tests'
	$(ECHO) '          clean ....... delete $(COLOR_SUMMARY)$(INSTALLDIR)/$(COLOR_NONE)'
	$(ECHO) '          help ........ print this help screen'
	$(ECHO)
